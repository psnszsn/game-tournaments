const express = require('express')
const Sequelize = require('sequelize')
const bodyParser = require('body-parser')
var request = require('request');

const app = express()
app.use('/', express.static('../frontend/dist/'))
app.use(/\/((?!igdb).)*/, bodyParser.json());

app.listen(8081)

// app.use(cors())
// app.use(bodyParser.json())
// app.use(express.static('../simple-app/build'))
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});



const sequelize = new Sequelize('profile', 'root', '', {
    dialect: "sqlite",
    host: "localhost",
    storage: 'database.sqlite'
})


app.post('/igdb/*', function (req, res) {    
    var newurl = 'https://api-v3.igdb.com/' + req.param(0);
    req.headers["user-key"] = "4f32636a2d67d84803e0f202b2d05476"
    req.pipe(request({ body: req.body, uri: newurl })).pipe(res);

});

const Game = sequelize.define('game', {
	name : Sequelize.STRING,
	igdb_id : Sequelize.INTEGER
}, {
	underscored : true
})

const Tournament = sequelize.define('tournament', {
	players : Sequelize.INTEGER,
	date : Sequelize.DATE
})

Game.hasMany(Tournament)


app.get('/create', async (req, res) => {
	try{
		await sequelize.sync({force : true})
		res.status(201).json({message : 'created'})
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.get('/games', async (req, res) => {
	try{
		let games = await Game.findAll()
		res.status(200).json(games)
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})


app.post('/games', async (req, res) => {
	try{
		//console.log(req.body)
		if (req.query.bulk && req.query.bulk == 'on'){
			await Game.bulkCreate(req.body)
			res.status(201).json({message : 'created'})
		}
		else{
			await Game.create(req.body)
			res.status(201).json({message : 'created'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.get('/games/:id', async (req, res) => {
	try{
		let game = await Game.findByPk(req.params.id)
		if (game){
			res.status(200).json(game)
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.put('/games/:id', async (req, res) => {
	try{
		let game = await Game.findByPk(req.params.id)
		if (game){
			await game.update(req.body)
			res.status(202).json({message : 'accepted'})
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.delete('/game/:id', async (req, res) => {
	try{
		let game = await Game.findByPk(req.params.id)
		if (game){
			await game.destroy()
			res.status(202).json({message : 'accepted'})
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.get('/games/:bid/tournaments', async (req, res) => {
	try{
		let game = await Game.findByPk(req.params.bid)
		if (game){
			let tournaments = await game.getTournaments()
			res.status(200).json(tournaments)
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.get('/games/:bid/tournaments/:cid', async (req, res) => {
	try{
		let game = await Game.findByPk(req.params.bid)
		if (game){
			let tournaments = await game.getTournaments({where : {id : req.params.cid}})
			res.status(200).json(tournaments.shift())
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.post('/games/:bid/tournaments', async (req, res) => {
	try{
		let game = await Game.findByPk(req.params.bid)
		if (game){
			let tournament = req.body
			tournament.game_id = game.id
			await Tournament.create(tournament)
			res.status(201).json({message : 'created'})
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.put('/games/:bid/tournaments/:cid', async (req, res) => {
	try{
		let game = await Game.findByPk(req.params.bid)
		if (game){
			let tournaments = await game.getTournaments({where : {id : req.params.cid}})
			let tournament = tournaments.shift()
			if (tournament){
				await tournament.update(req.body)
				res.status(202).json({message : 'accepted'})
			}
			else{
				res.status(404).json({message : 'not found'})
			}
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.delete('/games/:bid/tournaments/:cid', async (req, res) => {
	try{
		let game = await Game.findByPk(req.params.bid)
		if (game){
			let tournaments = await game.getTournaments({where : {id : req.params.cid}})
			let tournament = tournaments.shift()
			if (tournament){
				await tournament.destroy(req.body)
				res.status(202).json({message : 'accepted'})
			}
			else{
				res.status(404).json({message : 'not found'})
			}
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})


